#include <cstring>
#include <cstdlib>
#include <string>
#include <iostream>

#define _UNIX
#define  RARDLL
#include <dll.hpp>

HANDLE open_archive(char *file, char mode, RAROpenArchiveDataEx *archive_info) {
    HANDLE rarchive;

    archive_info->CmtBuf = NULL;
    if (mode == 'l')
        archive_info->OpenMode = RAR_OM_LIST;
    else if (mode == 'e')
        archive_info->OpenMode = RAR_OM_EXTRACT;
    else {
        std::cerr << "Unknown mode" << std::endl;
    }
    archive_info->ArcName = file;

    rarchive = RAROpenArchiveEx(archive_info);
    if (archive_info->OpenResult != 0) {
        std::cerr << "Error opening archive: " << file << "(" << archive_info->OpenResult << ")" << std::endl;
        if (RARCloseArchive(rarchive) != 0) {
            std::cerr << "Error: Could not close archive" << std::endl;
        }
        std::cerr << "Error: Could not open archive" << std::endl;
    }
    return rarchive;
}

void extract(HANDLE rarchive, const char *file, RARHeaderDataEx *file_info) {
    int retval(0);

    file_info->CmtBuf = NULL;
//    unsigned int i = 0;
    while(retval != ERAR_END_ARCHIVE) {
        retval = RARReadHeaderEx(rarchive, file_info);
        if (retval == ERAR_BAD_DATA) {
            std::cerr << "Error: RARReadHeaderEx: ERAR_BAD_DATA" << std::endl;
            break;
        }
        if (strcmp(file, file_info->FileName) == 0) {
            std::cerr << "Extracting " << file_info->FileName << ":";
            retval = RARProcessFile(rarchive, RAR_TEST, NULL, NULL);
            std::cerr << "done." << std::endl;
        } else {
            retval = RARProcessFile(rarchive, RAR_SKIP, NULL, NULL);
        }

        switch(retval) {
        case 0:
            return;
        case ERAR_END_ARCHIVE:
            std::cerr << "Error: RARProcessFile: ERAR_END_ARCHIVE" << std::endl;
            return;
        case ERAR_NO_MEMORY:
            std::cerr << "Error: RARProcessFile: ERAR_NO_MEMORY" << std::endl;
            return;
        case ERAR_BAD_DATA:
            std::cerr << "Error: RARProcessFile: ERAR_BAD_DATA" << std::endl;
            return;
        case ERAR_BAD_ARCHIVE:
            std::cerr << "Error: RARProcessFile: ERAR_BAD_ARCHIVE" << std::endl;
            return;
        case ERAR_UNKNOWN_FORMAT:
            std::cerr << "Error: RARProcessFile: ERAR_UNKNOWN_FORMAT" << std::endl;
            return;
        case ERAR_EOPEN:
            std::cerr << "Error: RARProcessFile: ERAR_EOPEN" << std::endl;
            return;
        case ERAR_ECREATE:
            std::cerr << "Error: RARProcessFile: ERAR_ECREATE" << std::endl;
            return;
        case ERAR_ECLOSE:
            std::cerr << "Error: RARProcessFile: ERAR_ECLOSE" << std::endl;
            return;
        case ERAR_EREAD:
            std::cerr << "Error: RARProcessFile: ERAR_EREAD" << std::endl;
            return;
        case ERAR_EWRITE:
            std::cerr << "Error: RARProcessFile: ERAR_EWRITE" << std::endl;
            return;
        case ERAR_SMALL_BUF:
            std::cerr << "Error: RARProcessFile: ERAR_SMALL_BUF" << std::endl;
            return;
        case ERAR_UNKNOWN:
            std::cerr << "Error: RARProcessFile: ERAR_UNKNOWN" << std::endl;
            return;
        case ERAR_MISSING_PASSWORD:
            std::cerr << "Error: RARProcessFile: ERAR_MISSING_PASSWORD" << std::endl;
            return;
        default:
            std::cerr << "Error: RARProcessFile: Really unknown error" << std::endl;
            return;
        }
//        i++;
    }
}

int CALLBACK rarCallBack(unsigned int msg, long, long p1, long p2) {
    switch(msg) {
    case UCM_CHANGEVOLUME:
    case UCM_CHANGEVOLUMEW:
        if (p2 == RAR_VOL_NOTIFY) {
            const char* f((const char*)p1);
            const size_t l(strlen(f));
            if (l != 1)
                std::cerr << f + (l > 3 ? l-3 : 0);
            return 1;
        } else {
            std::cerr << "Error: Volume not found." << (const char*)p1 << std::endl;
            return -1;
        }
    case UCM_PROCESSDATA:
//        std::cerr << ".";
        std::cout.write((const char*)p1, p2);
        return 1;
    case UCM_NEEDPASSWORD:
    case UCM_NEEDPASSWORDW:
        std::cerr << "Warning: Unimplemented case (UCM_NEEDPASSWORD)" << std::endl;
        return -1;
    default:
        std::cerr << "Warning: Unknown case: " << msg << std::endl;
        return -1;
    }
}

int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        std::cerr << "not enough args" << std::endl;
        return 1;
    }

    char* archive(argv[1]);
    char* file(argv[2]);
//    const size_t index(atoi(argv[2]));

    RAROpenArchiveDataEx archive_info;
    RARHeaderDataEx file_info;

    HANDLE rarchive = open_archive(archive, 'e', &archive_info);

    RARSetCallback(rarchive, rarCallBack, 0);

    extract(rarchive, file, &file_info);

    return 0;
}
