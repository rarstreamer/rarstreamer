#-------------------------------------------------
#
# Project created by QtCreator 2012-12-30T12:32:13
#
#-------------------------------------------------

QT       -= core gui

TARGET = rarstreamer
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../test/rar/release/ -lunrar
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../test/rar/debug/ -lunrar
#else:symbian: LIBS += -lunrar
LIBS += $$PWD/unrar/libunrar.so

INCLUDEPATH += $$PWD/unrar
DEPENDPATH += $$PWD/unrar
